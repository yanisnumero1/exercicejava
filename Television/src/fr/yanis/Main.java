package fr.yanis;

import fr.yanis.television.*;
import fr.yanis.television.exception.TailleEcranException;
import fr.yanis.television.exception.TailleStringException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {

    public static void main(String[] args){

        Samsung samsung= new Samsung(12.1f, 2400, "hdmi", 30, true);
        Benq benq = new Benq (12.1f, 2400, "hdmi", 30, true);
        Philips philips = new Philips (12.1f, 2400, "hdmi", 30, true);
        Sony sony = new Sony(12.1f, 2400, "hdmi", 30, "egggggggg");

        List<Television> listTelevision = new ArrayList<>();

        listTelevision.add(samsung);
        listTelevision.add(benq);
        listTelevision.add(philips);

        Iterator<Television> iterator= listTelevision.iterator();

        Television tele;

        while(iterator.hasNext()) {
            tele = iterator.next();
            System.out.println(tele.toString());
            tele.ManiereAllumerEteindreTV();
            System.out.println("--------------------------------------------");


        }
        try{
        samsung.verifTailleEcran();
            System.out.println("il n'ya pas d'erreur");
    }catch(TailleEcranException e){
            System.out.println(e.getMessage());
        }
}
}
