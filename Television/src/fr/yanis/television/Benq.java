package fr.yanis.television;

public class Benq extends Television {
    private boolean dvi;

    public Benq(float tailleEcran, int resolution, String connectics, int remote, boolean dvi) {
        super(tailleEcran, resolution, connectics, remote);
        this.dvi = dvi;
    }

    public boolean isDvi() {
        return dvi;
    }

    public void setDvi(boolean dvi) {
        this.dvi = dvi;
    }
    protected float getTailleEcran() {
        return super.getTailleEcran();
    }

    protected void setTailleEcran(float tailleEcran) {
        super.setTailleEcran(tailleEcran);
    }

    protected int getResolution() {
        return super.getResolution();
    }

    protected void setResolution(int resolution) {
        super.setResolution(resolution);
    }

    protected String getConnectics() {
        return super.getConnectics();
    }

    protected void setConnectics(String connectics) {
        super.setConnectics(connectics);
    }

    protected int getRemote() {
        return super.getRemote();
    }

    protected void setRemote(int remote) {
        super.setRemote(remote);
    }

    protected boolean isEtat() {
        return super.isEtat();
    }

    public void setEtat(boolean etat) {
        super.setEtat(etat);
    }

    @Override
    public void ManiereAllumerEteindreTV() {
        System.out.println("allume avec lordi");

    }

    @Override
    public String toString() {
        return "Benq{" +
                "dvi=" + dvi +
                '}';
    }

}
