package fr.yanis.television;

public class Philips extends Television {
    private boolean bluetooth;

    public Philips(float tailleEcran, int resolution, String connectics, int remote, boolean bluetooth) {
        super(tailleEcran, resolution, connectics, remote);
        this.bluetooth = bluetooth;
    }

    public boolean isBluetooth() {
        return bluetooth;
    }

    public void setBluetooth(boolean bluetooth) {
        this.bluetooth = bluetooth;
    }

    @Override
    public String toString() {
        return "Philips{" +
                "bluetooth=" + bluetooth +
                "} " + super.toString();
    }
    protected float getTailleEcran() {
        return super.getTailleEcran();
    }

    protected void setTailleEcran(float tailleEcran) {
        super.setTailleEcran(tailleEcran);
    }

    protected int getResolution() {
        return super.getResolution();
    }

    protected void setResolution(int resolution) {
        super.setResolution(resolution);
    }

    protected String getConnectics() {
        return super.getConnectics();
    }

    protected void setConnectics(String connectics) {
        super.setConnectics(connectics);
    }

    protected int getRemote() {
        return super.getRemote();
    }

    protected void setRemote(int remote) {
        super.setRemote(remote);
    }

    protected boolean isEtat() {
        return super.isEtat();
    }

    public void setEtat(boolean etat) {
        super.setEtat(etat);
    }

    @Override
    public void ManiereAllumerEteindreTV() {
        System.out.println("allume avec telephone");

    }
}
