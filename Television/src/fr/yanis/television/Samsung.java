package fr.yanis.television;

import fr.yanis.television.exception.TailleEcranException;
import fr.yanis.television.exception.TailleStringException;

public class Samsung extends Television {
    private boolean wifi;

    public Samsung(float tailleEcran, int resolution, String connectics, int remote, boolean wifi) {
        super(tailleEcran, resolution, connectics, remote);
        this.wifi = wifi;
    }
    protected float getTailleEcran() {
        return super.getTailleEcran();
    }

    protected void setTailleEcran(float tailleEcran) {
        super.setTailleEcran(tailleEcran);
    }

    protected int getResolution() {
        return super.getResolution();
    }

    protected void setResolution(int resolution) {
        super.setResolution(resolution);
    }

    protected String getConnectics() {
        return super.getConnectics();
    }

    protected void setConnectics(String connectics) {
        super.setConnectics(connectics);
    }

    protected int getRemote() {
        return super.getRemote();
    }

    protected void setRemote(int remote) {
        super.setRemote(remote);
    }

    protected boolean isEtat() {
        return super.isEtat();
    }

    public void setEtat(boolean etat) {
        super.setEtat(etat);
    }

    @Override
    public void ManiereAllumerEteindreTV() {
        System.out.println("allume avec la commande");

    }

    public boolean isWifi() {
        return wifi;
    }

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    @Override
    public String toString() {
        return "Samsung{" +
                "wifi=" + wifi + super.toString();
    }

    public void verifTailleEcran() throws TailleEcranException {
        if(this.getTailleEcran()<13){
            throw new TailleEcranException("la taille de l'écran est inférieur à 13");
        }else {
            System.out.println("la taille de l'écran est de "+ this.getTailleEcran());
        }
    }
}
