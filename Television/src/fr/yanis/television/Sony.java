package fr.yanis.television;

import fr.yanis.television.exception.TailleStringException;

public class Sony extends Television {
    private String magnetoscope;


    public Sony(float tailleEcran, int resolution, String connectics, int remote, String magnetoscope){
        super(tailleEcran, resolution, connectics, remote);
        this.magnetoscope=magnetoscope;
    }


    protected float getTailleEcran() {
        return super.getTailleEcran();
    }

    protected void setTailleEcran(float tailleEcran) {
         super.setTailleEcran(tailleEcran);
    }

    protected int getResolution() {
        return super.getResolution();
    }

    protected void setResolution(int resolution) {
        super.setResolution(resolution);
    }

    protected String getConnectics() {
        return super.getConnectics();
    }

    public String isMagnetoscope() throws TailleStringException {
        if(magnetoscope.length()<2){
            throw new TailleStringException("La chaine de caractère est inferieur a 2 ");
        }else{
            return magnetoscope;
        }

    }

    public void setMagnetoscope(String magnetoscope) {
        this.magnetoscope = magnetoscope;
    }

    protected void setConnectics(String connectics) {
        super.setConnectics(connectics);
    }

    protected int getRemote() {
        return super.getRemote();
    }

    protected void setRemote(int remote) {
        super.setRemote(remote);
    }

    protected boolean isEtat() {
        return super.isEtat();
    }

    public void setEtat(boolean etat) {
        super.setEtat(etat);
    }

    @Override
    public String toString() {
        return "Sony{" +
                "magnetoscope=" + magnetoscope +
                "} " + super.toString();
    }

    @Override
    public void ManiereAllumerEteindreTV() {
        System.out.println("allume avec la telecommande");
    }
}
