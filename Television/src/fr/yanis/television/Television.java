package fr.yanis.television;

public abstract class Television {

    private float tailleEcran;
    private int resolution;
    public String connectics;
    private int remote;
    private boolean etat;

    public Television(float tailleEcran, int resolution, String connectics, int remote){
        this.tailleEcran=tailleEcran;
        this.resolution=resolution;
        this.connectics=connectics;
        this.remote=remote;
    }

    @Override
    public String toString() {
        return "{tailleEcran=" + tailleEcran +
                ", resolution=" + resolution +
                ", connectics='" + connectics + '\'' +
                ", remote=" + remote +"}";
    }

    protected float getTailleEcran() {
        return tailleEcran;
    }

    protected void setTailleEcran(float tailleEcran) {
        this.tailleEcran = tailleEcran;
    }

    protected int getResolution() {
        return resolution;
    }

    protected void setResolution(int resolution) {
        this.resolution = resolution;
    }

    protected String getConnectics() {
        return connectics;
    }

    protected void setConnectics(String connectics) {
        this.connectics = connectics;
    }

    protected int getRemote() {
        return remote;
    }

    protected void setRemote(int remote) {
        this.remote = remote;
    }

    protected boolean isEtat() {
        return etat;
    }

    protected void setEtat(boolean etat) {
        this.etat = etat;
    }

    public abstract void ManiereAllumerEteindreTV();
}
