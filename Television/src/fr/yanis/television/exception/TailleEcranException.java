package fr.yanis.television.exception;

public class TailleEcranException extends Exception{

    private String message;

    public TailleEcranException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
