package fr.yanis.television.exception;

public class TailleStringException extends Exception {

    private String message;

    public TailleStringException(String e){
        this.message=e;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
