package fr.yanis;

import fr.yanis.consoles.*;
import fr.yanis.consoles.exception.TaillePuissanceException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        PlayStation4 play4 = new PlayStation4(4,10, "HDMI", true );
        Playstation5 play5 = new Playstation5(4, 10, "HDMI 4K", true);
        SuperNintendo superN = new SuperNintendo(2, 3, "Peritel", false);
        UnixBox xbox = new UnixBox(2, 3, "HDMI", true);

        List<Consoles>listConsoles = new ArrayList<>();
        listConsoles.add(play4);
        listConsoles.add(play5);
        listConsoles.add(superN);
        listConsoles.add(xbox);

        Iterator<Consoles> iterator= listConsoles.iterator();

        Consoles consoles;

        while(iterator.hasNext()){
            consoles=iterator.next();
            System.out.println(consoles.toString());
            consoles.AllumerConsole();
            consoles.SupportJeux();
            System.out.println("_________________________");
        }
        try{
            play4.VerifTaillePuissance();
            System.out.println("Il n'y a pas d'erreur");

        } catch (TaillePuissanceException e) {
            System.out.println(e.getMessage());
        }

    }
}
