package fr.yanis.consoles;

import fr.yanis.consoles.exception.TaillePuissanceException;

public abstract class Consoles {

    int nbrManette;
    int puissance;
    String connectics;
    boolean hd;


    protected Consoles(int nbrManette, int puissance, String connectics, boolean hd){
        this.nbrManette=nbrManette;
        this.puissance=puissance;
        this.connectics =connectics;
        this.hd=hd;
    }

    public int getNbrManette() {
        return nbrManette;
    }

    public void setNbrManette(int nbrManette) {
        this.nbrManette = nbrManette;
    }

    public int getPuissance() {
        return puissance;
    }

    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }

    public String getConnectics() {
        return connectics;
    }

    public void setConnectics(String connectics) {
        this.connectics = connectics;
    }

    public boolean isHd() {
        return hd;
    }

    public void setHd(boolean hd) {
        this.hd = hd;
    }

    @Override
    public String toString() {
        return  "{nbrManette=" + nbrManette +
                ",possede hd=" + hd +
                ", puissance=" + puissance +
                ", cable='" + connectics + '\'' +
                '}';
    }

    public abstract void SupportJeux();

    public abstract void AllumerConsole();

    public abstract void VerifTaillePuissance() throws TaillePuissanceException;


}
