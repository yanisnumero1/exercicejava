package fr.yanis.consoles;

import fr.yanis.consoles.exception.TaillePuissanceException;

public class Playstation5 extends Consoles {

    public Playstation5(int nbrManette, int puissance, String cable, boolean hd) {
        super(nbrManette, puissance, cable, hd);
    }

    public int getNbrManette() {
        return nbrManette;
    }

    public void setNbrManette(int nbrManette) {
        this.nbrManette = nbrManette;
    }

    public int getPuissance() {
        return puissance;
    }

    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }

    public String getConnectics() {
        return connectics;
    }

    public void setConnectics(String connectics) {
        this.connectics = connectics;
    }

    public boolean isHd() {
        return hd;
    }

    public void setHd(boolean hd) {
        this.hd = hd;
    }

    @Override
    public String toString() {
        return "Playstation{} " + super.toString();
    }

    @Override
    public void SupportJeux() {
        System.out.println("Je fonctionne avec des CD et des jeux en mémoire");
    }

    @Override
    public void AllumerConsole() {
        System.out.println("je m'allume avec la manette ou le bouton");
    }

    @Override
    public void VerifTaillePuissance() throws TaillePuissanceException {
        if(this.getPuissance()>10){
            throw new TaillePuissanceException("la puissance est trop elevée");
        }else{
            System.out.println("la taille de la puissance est de "+ this.getPuissance());
        }
    }
}
