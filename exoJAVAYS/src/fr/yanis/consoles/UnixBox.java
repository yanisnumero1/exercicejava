package fr.yanis.consoles;

import fr.yanis.consoles.exception.TaillePuissanceException;

public class UnixBox extends Consoles {

    public UnixBox(int nbrManette, int puissance, String connectics, boolean hd) {
        super(nbrManette, puissance, connectics, hd);
    }

    @Override
    public String toString() {
        return "UnixBox{} " + super.toString();
    }

    public int getNbrManette() {
        return nbrManette;
    }

    public void setNbrManette(int nbrManette) {
        this.nbrManette = nbrManette;
    }

    public int getPuissance() {
        return puissance;
    }

    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }

    public String getConnectics() {
        return connectics;
    }

    public void setConnectics(String connectics) {
        this.connectics = connectics;
    }

    public boolean isHd() {
        return hd;
    }

    public void setHd(boolean hd) {
        this.hd = hd;
    }

    @Override
    public void SupportJeux() {
        System.out.println("Je fonctionne avec des jeux en mémoire");

    }

    @Override
    public void AllumerConsole() {
        System.out.println("On m'allume avec la telecommande");

    }

    @Override
    public void VerifTaillePuissance() throws TaillePuissanceException {
        if(this.getPuissance()>3){
            throw new TaillePuissanceException("la puissance est trop elevée");
        }else{
            System.out.println("la taille de la puissance est de "+ this.getPuissance());
        }
    }
}
